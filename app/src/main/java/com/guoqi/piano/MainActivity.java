package com.guoqi.piano;

import android.app.Activity;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.HashMap;

public class MainActivity extends Activity implements View.OnClickListener {

    SoundPool sp; //得到一个声音池引用
    HashMap spMap; //得到一个map的引用

    private TextView
            tv_d1,
            tv_d2,
            tv_d3,
            tv_d4,
            tv_d5,
            tv_d6,
            tv_d7,

    tv_1,
            tv_2,
            tv_3,
            tv_4,
            tv_5,
            tv_6,
            tv_7,

    tv_g1,
            tv_g2,
            tv_g3,
            tv_g4,
            tv_g5,
            tv_g6,
            tv_g7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initSoundPool(); //初始化声音池
        initUI();
    }

    private void initUI() {
        tv_d1 = (TextView) findViewById(R.id.tv_d1);
        tv_d2 = (TextView) findViewById(R.id.tv_d2);
        tv_d3 = (TextView) findViewById(R.id.tv_d3);
        tv_d4 = (TextView) findViewById(R.id.tv_d4);
        tv_d5 = (TextView) findViewById(R.id.tv_d5);
        tv_d6 = (TextView) findViewById(R.id.tv_d6);
        tv_d7 = (TextView) findViewById(R.id.tv_d7);


        tv_1 = (TextView) findViewById(R.id.tv_1);
        tv_2 = (TextView) findViewById(R.id.tv_2);
        tv_3 = (TextView) findViewById(R.id.tv_3);
        tv_4 = (TextView) findViewById(R.id.tv_4);
        tv_5 = (TextView) findViewById(R.id.tv_5);
        tv_6 = (TextView) findViewById(R.id.tv_6);
        tv_7 = (TextView) findViewById(R.id.tv_7);

        tv_g1 = (TextView) findViewById(R.id.tv_g1);
        tv_g2 = (TextView) findViewById(R.id.tv_g2);
        tv_g3 = (TextView) findViewById(R.id.tv_g3);
        tv_g4 = (TextView) findViewById(R.id.tv_g4);
        tv_g5 = (TextView) findViewById(R.id.tv_g5);
        tv_g6 = (TextView) findViewById(R.id.tv_g6);
        tv_g7 = (TextView) findViewById(R.id.tv_g7);


        tv_1.setOnClickListener(this);
        tv_2.setOnClickListener(this);
        tv_3.setOnClickListener(this);
        tv_4.setOnClickListener(this);
        tv_5.setOnClickListener(this);
        tv_6.setOnClickListener(this);
        tv_7.setOnClickListener(this);

        tv_d1.setOnClickListener(this);
        tv_d2.setOnClickListener(this);
        tv_d3.setOnClickListener(this);
        tv_d4.setOnClickListener(this);
        tv_d5.setOnClickListener(this);
        tv_d6.setOnClickListener(this);
        tv_d7.setOnClickListener(this);

        tv_g1.setOnClickListener(this);
        tv_g2.setOnClickListener(this);
        tv_g3.setOnClickListener(this);
        tv_g4.setOnClickListener(this);
        tv_g5.setOnClickListener(this);
        tv_g6.setOnClickListener(this);
        tv_g7.setOnClickListener(this);
    }

    /**
     * //maxStreams参数，该参数为设置同时能够播放多少音效
     * //streamType参数，该参数设置音频类型，在游戏中通常设置为：STREAM_MUSIC
     * //srcQuality参数，该参数设置音频文件的质量，目前还没有效果，设置为0为默认值。
     */
    private void initSoundPool() {
        sp = new SoundPool(6, AudioManager.STREAM_MUSIC, 0);

        spMap = new HashMap<Integer, Integer>();
        spMap.put(1, sp.load(this, R.raw.d1, 1));
        spMap.put(2, sp.load(this, R.raw.d2, 1));
        spMap.put(3, sp.load(this, R.raw.d3, 1));
        spMap.put(4, sp.load(this, R.raw.d4, 1));
        spMap.put(5, sp.load(this, R.raw.d5, 1));
        spMap.put(6, sp.load(this, R.raw.d6, 1));
        spMap.put(7, sp.load(this, R.raw.d7, 1));

        spMap.put(11, sp.load(this, R.raw.z1, 1));
        spMap.put(22, sp.load(this, R.raw.z2, 1));
        spMap.put(33, sp.load(this, R.raw.z3, 1));
        spMap.put(44, sp.load(this, R.raw.z4, 1));
        spMap.put(55, sp.load(this, R.raw.z5, 1));
        spMap.put(66, sp.load(this, R.raw.z6, 1));
        spMap.put(77, sp.load(this, R.raw.z7, 1));

        spMap.put(111, sp.load(this, R.raw.g1, 1));
        spMap.put(222, sp.load(this, R.raw.g2, 1));
        spMap.put(333, sp.load(this, R.raw.g3, 1));
        spMap.put(444, sp.load(this, R.raw.g4, 1));
        spMap.put(555, sp.load(this, R.raw.g5, 1));
        spMap.put(666, sp.load(this, R.raw.g6, 1));
        spMap.put(777, sp.load(this, R.raw.g7, 1));
    }


    public void playSound(int sound, int number) {    //播放声音,参数sound是播放音效的id，参数number是播放音效的次数
        AudioManager am = (AudioManager) this.getSystemService(this.AUDIO_SERVICE);//实例化AudioManager对象
        float audioMaxVolumn = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);  //返回当前AudioManager对象的最大音量值
        float audioCurrentVolumn = am.getStreamVolume(AudioManager.STREAM_MUSIC);//返回当前AudioManager对象的音量值
        float volumnRatio = audioCurrentVolumn / audioMaxVolumn;
        sp.play(
                (int) spMap.get(sound),                   //播放的音乐id
                volumnRatio,                        //左声道音量
                volumnRatio,                        //右声道音量
                1,                                  //优先级，0为最低
                number,                             //循环次数，0无不循环，-1无永远循环
                1                                   //回放速度 ，该值在0.5-2.0之间，1为正常速度
        );
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_1:
                playSound(11, 0);
                break;
            case R.id.tv_2:
                playSound(22, 0);
                break;
            case R.id.tv_3:
                playSound(33, 0);
                break;
            case R.id.tv_4:
                playSound(44, 0);
                break;
            case R.id.tv_5:
                playSound(55, 0);
                break;
            case R.id.tv_6:
                playSound(66, 0);
                break;
            case R.id.tv_7:
                playSound(77, 0);
                break;


            case R.id.tv_d1:
                playSound(1, 0);
                break;
            case R.id.tv_d2:
                playSound(2, 0);
                break;
            case R.id.tv_d3:
                playSound(3, 0);
                break;
            case R.id.tv_d4:
                playSound(4, 0);
                break;
            case R.id.tv_d5:
                playSound(5, 0);
                break;
            case R.id.tv_d6:
                playSound(6, 0);
                break;
            case R.id.tv_d7:
                playSound(7, 0);
                break;

            case R.id.tv_g1:
                playSound(111, 0);
                break;
            case R.id.tv_g2:
                playSound(222, 0);
                break;
            case R.id.tv_g3:
                playSound(333, 0);
                break;
            case R.id.tv_g4:
                playSound(444, 0);
                break;
            case R.id.tv_g5:
                playSound(555, 0);
                break;
            case R.id.tv_g6:
                playSound(666, 0);
                break;
            case R.id.tv_g7:
                playSound(777, 0);
                break;
        }
    }
}
